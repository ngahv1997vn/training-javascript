// EX1
class Customer {
    constructor(name, isMember, type) {
        this.name = name;
        this.isMember = isMember;
        this.type = type;
    }
}

// EX2
class DiscountRate {
    constructor() {
        this.serviceDiscount = { Premium: 0.2, Gold: 0.15, Silver: 0.1 }
        this.productDiscount = { Premium: 0.1, Gold: 0.1, Silver: 0.1 }
    }
    static serviceDiscountRate(type) {
        let rate = this.serviceDiscount[type]
        return rate !== undefined ? rate : 0
    }
    static productDiscountRate(type) {
        let rate = this.productDiscount[type]
        return rate !== undefined ? rate : 0
    }
}

// EX3
class Visit {
    constructor(customer, dateVisit) {
        this.customer = customer;
        this.dateVisit = dateVisit;
        this.serviceExpense = 0;
        this.productExpense = 0;
    }

    setServiceeExpense(money) {
        if (this.customer.isMember == false)
            this.serviceExpense = money;
        else {
            this.serviceExpense = money - money * DiscountRate.serviceDiscountRate(this.customer.type);
        }
    }

    setProductExpense(money) {
        if (this.customer.isMember == false)
            this.productExpense = money;
        else {
            this.productExpense = money - money * DiscountRate.productDiscountRate(this.customer.type);
        }
    }

    get totalExpense() {
        return this.serviceExpense + this.productExpense;
    }
}

