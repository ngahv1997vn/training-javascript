class Contact {
    constructor(id, name, phoneNumber) {
        this.id = id
        this.name = name
        this.phoneNumber = phoneNumber
    }
}
class PhoneBook {
    constructor(id, listContact = []) {
        this.id = id
        this.listContact = listContact
    }
    insert(id, name, phoneNumber) {
        // this.listContact.push({ 'name': name, 'phoneNumber': phoneNumber })
        let contact = new Contact(id, name, phoneNumber);
        this.listContact.push(contact);
    }
    lookUp(str) {
        let arr = []
        this.listContact.forEach(element => {
            if (element.name.includes(str) || element.phoneNumber.includes(str)) {
                arr.push(element);
            }
        });
        return arr;
        // return this.listContact.filter(element => element.name.includes(str) || element.phoneNumber.includes(str))
    }
    update(id, name, phoneNumber) {
        this.listContact.forEach((element) => {
            if (id === element.id) {
                element.name = name;
                element.phoneNumber = phoneNumber;
            }
        });
    }
    delete(id) {
        this.listContact.forEach((element, index) => {
            if (id === element.id) this.listContact.splice(index, 1)
        })
    }
}
let pb = new PhoneBook(1);
//console.log(pb.listContact); // Before Insert
pb.insert(1, 'Nga', '01214508421')
pb.insert(2, 'Linh', '0123456789')
pb.insert(4, 'Linh', '0123456789')
//console.log(pb.listContact); // After Insert
// console.log(pb.lookUp('0123')); // Lockup by Phone Number
// console.log(pb.lookUp('Li')); // Lockup by Name
// console.log(pb.listContact); // Before Update
pb.update(2, "Linh", "0987654321");
// console.log(pb.listContact); // After Updater
pb.delete(1)
// console.log(pb.listContact); // After Delete



