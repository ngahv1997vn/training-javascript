class Personal {
    constructor(ID, fullName, email, birthday, position, seniority) {
        this.ID = ID;
        this.fullName = fullName;
        this.birthday = birthday;
        this.position = position;
        this.email = email;
        this.seniority = seniority;
    }
    get salary() {
        let salaryByPosition = {
            manager: 1000,
            secterary: 400,
            employee: 300
        }
        let basicSalary = salaryByPosition[this.position.toLowerCase()];
        //Validate
        if (basicSalary == undefined) {
            return "nhap sai roi ong noi";
        }

        switch (true) {
            case (this.seniority < 12):
                return basicSalary * 1.2
            case (this.seniority < 24):
                return basicSalary * 2
            case (this.seniority < 36):
                return basicSalary * 3.4
            default:
                return basicSalary * 4.5
        }
    }

    get Age() {
        let age = 0;
        let cur = new Date();
        age = cur.getFullYear() - this.birthday.year
        if (this.birthday.month > cur.getMonth() + 1 || this.birthday.month == cur.getMonth() + 1 && this.birthday.day > cur.getDate()) {
            age--;
        }
        return age;
    }
    static personalHighest(personal) {
        let arr = [];
        personal = personal.sort((slry1, slry2) => slry1.salary < slry2.salary);
        // personal.forEach((element) => {
        //     if (personal[0].salary == element.salary) {
        //         arr.push(element);
        //     }
        // });
        for (let i = 0; i < personal.length; i++) {
            if (personal[0].salary == personal[i].salary)
                arr.push(personal[i]);
            else return arr;
        }
    }
}
let Personal1 = new Personal(1, "thanh ha", "hanguyen@gmail.com", { day: 10, month: 1, year: 1999 }, "EMPLOYEE", 4)
let Personal2 = new Personal(1, "thanh ha", "hanguyen@gmail.com", { day: 10, month: 1, year: 1999 }, "manager", 5)
let Personal3 = new Personal(1, "thanh ha", "hanguyen@gmail.com", { day: 10, month: 1, year: 1999 }, "manager", 99)

console.log(Personal.personalHighest([Personal1, Personal2, Personal3]));



// Inheritance Exercise
class Person {
    constructor(firstName, fullName, birthday, gender) {
        this.firstName = firstName
        this.fullName = fullName
        this.birthday = birthday
        this.gender = gender
    }
    get getAge() {
        let age1 = 0
        let current1 = new Date();
        age1 = current1.getFullYear - this.birthday.year
        if (this.birthday.month > current1.getMonth() + 1 || this.birthday.month > current1.getMonth(+1) && this.birthday.date > current1.getDate()) {
            age1--;
        }
        return age1;
    }
}
let Person1 = new Person("Dong", "Phuoc Duc", { date: 12, month: 01, year: 1999 }, "male")

class Employer extends Person {
    constructor(position, company, workHours) {
        super(firstName, fullName, birthday, gender)
        this.position = position;
        this.company = company;
        this.workHours = workHours;
    }
    get salary() {
        let Salary = {
            manager: 10,
            secterary: 7,
            employee: 4
        }
        let BasicSalary = Salary[this.position]
        return BasicSalary * this.workHours;
    }
}
let Employer1 = new Employer("manager", "FPT", 20)